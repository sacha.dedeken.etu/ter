
#### Fichier de transformations des données Wikipédia en base de données

import xml.etree.ElementTree as etree
import codecs
import csv
import time
import os
import sqlite3
import timeit
import re

#### 1) Parseur en flux et extraction de lien vers un fichier CSV

dump = "data/wiki/frwiki-20190920-pages-articles-multistream.xml"
context = etree.iterparse(dump)
stock_liens = "data/wiki/stock_liens.csv"

# regex_lien = re.compile("\[\[[^\[]+\]\]")
# stop_words = ["Fichier:","Catégorie:","Image:","File:","Aide:","fichier:","file:","catégorie:","PHP:","image:","wikt:","Wikibooks:","wikilivres:"]
#
# def link_extract(texte):
#     """
#         Prend en entrée un texte de page Wikipédia et renvoie une liste des noms
#         de page liées aux liens de ce texte.
#     """
#     try:
#         liste_liens = regex_lien.findall(texte)
#     except:
#         return []
#     liste_liens = list(map(lambda x:x[2:-2].split("|")[0].split("#")[0], liste_liens))
#     liste_liens = list(filter(lambda x:not(any(y in x for y in stop_words)), liste_liens))
#     return liste_liens
#
# with open(stock_liens, "w+", encoding="utf8",newline="") as sl:
#     writer = csv.writer(sl)
#     lignes = []
#     for i,(action, elem) in enumerate(context):
#             if i%1001 == 0:
#                 writer.writerows(lignes)
#                 lignes = []
#             if "page" in elem.tag:
#                 titre = next(filter(lambda e:"title" in e.tag, elem.findall("*"))).text
#                 texte = next(filter(lambda e:"text" in e.tag, elem.findall("**"))).text
#                 liens = link_extract(texte)
#                 lignes.append([titre]+liens)
#                 elem.clear()
#             if i%100001 == 0:
#                 print(i)
#             else:
#                 pass

#### 2) passage en base de données

bdd = "data/wiki/base_wiki.db"
conn = sqlite3.connect(bdd)
cursor = conn.cursor()

# création de la table des noeuds

cursor.execute("""
CREATE TABLE IF NOT EXISTS wiki_noeud(
     id INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,
     noeud TEXT)
""")
conn.commit()

with open(stock_liens, "r+", encoding="utf8") as sl:
    dr = csv.DictReader(sl, fieldnames=["noeud"], restkey="list_liens")
    to_db = [(i,j['noeud']) for i,j in enumerate(dr)]
print("extraction du CSV noeud finie !")
cursor.executemany("INSERT INTO wiki_noeud (id,noeud) VALUES (?,?);", to_db)
print("insertion des noeuds finie !")
conn.commit()
conn.close()
to_db = []

# création de la table des relations

conn = sqlite3.connect(bdd)
cursor = conn.cursor()

cursor.execute("""
CREATE TABLE IF NOT EXISTS wiki_relation(
     id_d INTEGER,
     id_a INTEGER)
""")
conn.commit()

cursor.execute("SELECT * FROM wiki_noeud")
rows = cursor.fetchall()
dico = dict()
for row in rows:
    dico[row[1]] = row[0]
print("dico créé !")

with open(stock_liens, "r+", encoding="utf8") as sl:
    dr = csv.DictReader(sl, fieldnames=["noeud"], restkey="list_liens")
    waiting = []
    for row in dr:
        if len(waiting) > 500000:
            cursor.executemany("INSERT INTO wiki_relation (id_d,id_a) VALUES (?,?);", waiting)
            conn.commit()
            waiting = []
            print('insertion 500 000 relations')
        id_d = dico[row["noeud"]]
        if "list_liens" in row.keys():
            for lien in row["list_liens"]:
                if lien in dico.keys():
                    waiting.append((id_d, dico[lien]))
