
##### Fichier de transformations des données JeuxDeMots en base de données

import codecs
import csv
import sqlite3

##### les fichiers

jdm = "data/jdm/01012020-LEXICALNET-JEUXDEMOTS-FR-NOHTML.txt"
noeuds = "data/jdm/noeuds.txt"
relations = "data/jdm/relations.csv"

##### 1) extraction des noeuds intéressants

# with open(jdm, "r+", encoding="cp1252", errors="replace") as f, open(noeuds, "w+", encoding="utf8") as n:
#     noeud = False
#     relation = False
#     for i,line in enumerate(f):
#         if "// -- NODES" in line:
#             noeud = True
#         if "// -- RELATIONS" in line:
#             relation = True
#         elif noeud:
#             if relation:
#                 break
#             if "eid=" in line:
#                 ligne = [x[1] for x in list(map(lambda x:x.split("="), line.split("|nf=")[0].split("\n")[0].split("|"))) if x[0] in ["eid","n","t","w"]]
#                 # transforme "eid=24808|n="mégalopole"|t=1|w=104|nf="blablabla9876"" en "24808,"mégalopole",1,104"
#                 if (ligne[2] == "1" or ligne[2] == "777") and int(ligne[3]) > 2:
#                 # on garde (t=1 ou t=777) et (w > 2)
#                     n.write(",".join(ligne)+"\n")

##### 2) extraction des relations

# with open("data/jdm/noeuds.txt", "r+", encoding="utf8") as n:
#     ensemble_eid = set()
#     for line in n:
#         ensemble_eid.add(int(line.split(",")[0]))
#     print("ensemble_eid fait :", len(ensemble_eid))
#
# with open(jdm, "r+", encoding="cp1252", errors="replace") as f, open(relations, "w+", newline="") as r:
#     relation = False
#     writer = csv.writer(r)
#     writer.writerow(["rid","n1","n2","t","w"])
#     i=1
#     lignes = []
#     for line in f:
#         if "// -- RELATIONS" in line:
#             relation = True
#         elif relation:
#             if "rid=" in line:
#                 ligne = [int(x[1]) for x in list(map(lambda x:x.split("="), line.split("|"))) if x[0] in ["rid","n1","n2","t","w"]]
#                 #print(line,ligne)
#                 if ligne[3] == 0 and ligne[4] > 2:
#                     if ligne[1] in ensemble_eid and ligne[2] in ensemble_eid:
#                         lignes.append(ligne)
#                         i+=1
#             if i%10000 == 0:
#                 print("GG SACHA !!", i)
#                 writer.writerows(lignes)
#                 lignes = []

##### 3) passage en base de données

bdd = "data/jdm/base_jeuxdemots.db"
conn = sqlite3.connect(bdd)
cursor = conn.cursor()

# cursor.execute("""
# CREATE TABLE IF NOT EXISTS jm_noeud(
#      eid INTEGER PRIMARY KEY,
#      n TEXT,
#      t INTEGER,
#      w INTEGER);
# """)
#
# with open("data/jdm/noeuds.csv",'r', encoding="utf8") as fin: # `with` statement available in 2.5+
#     # csv.DictReader uses first line in file for column headings by default
#     dr = csv.DictReader(fin) # comma is default delimiter
#     to_db = [(i['eid'], i['n'],i['t'],i['w']) for i in dr]
# cursor.executemany("INSERT INTO jm_noeud (eid,n,t,w) VALUES (?, ?,?,?);", to_db)
# conn.commit()

cursor.execute("""
CREATE TABLE IF NOT EXISTS jm_relation(
     rid INTEGER PRIMARY KEY,
     n1 INTEGER,
     n2 INTEGER,
     t INTEGER,
     w INTEGER);
""")

with open("data/jdm/relations.csv",'r', encoding="utf8") as fin: # `with` statement available in 2.5+
    # csv.DictReader uses first line in file for column headings by default
    dr = csv.DictReader(fin) # comma is default delimiter
    to_db = [(i['rid'], i['n1'],i['n2'],i['t'],i['w']) for i in dr]
cursor.executemany("INSERT INTO jm_relation (rid,n1,n2,t,w) VALUES (?,?,?,?,?);", to_db)
conn.commit()
conn.close()







#
