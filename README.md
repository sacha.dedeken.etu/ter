**Intitulé :** Analyse topologique discrète et continue d'un réseau sémantique

**Résumé :** 
Les réseaux sémantiques sont des objets riches et interdisciplinaires qui permettent de s'intéresser à une certaine structure (les réseaux) d'un certain type d'objet et de relations (la sémantique) et relèvent aussi bien de la linguistique que de l'informatique ou de la psychologie, autrement dit des sciences cognitives. Dans le Traitement Automatique des Langues (TAL / NLP), nous pouvons voir un réseau sémantique (i.e. Wikipédia) selon deux approche/topologie :
1. Soit selon l'approche vectorielle/continue (chaque page-concept est un vecteur qui est représenté dans un espace-dictionnaire de très grande dimension, selon les cooccurences de mots présentes dans son texte associé) 
2. Soit selon l'approche en graphe/discrète (chaque page-concept est un noeud relié à d'autres dans un graphe orienté par les liens hypertextes présents dans son texte associé).

Il est ensuite possible d'analyser ces nouveaux objets pour en tirer des informations :
1. En analysant la similarité entre les vecteurs-pages et via des techniques de *clustering*
2. En calculant des mesures de centralité du graphe (*degree centrality*, *betweenness centrality* approximée via sampling) [[1]](https://i11www.iti.kit.edu/extra/publications/bf-cmbcf-05.pdf) [[2]](https://networkx.github.io/documentation/stable/reference/algorithms/generated/networkx.algorithms.centrality.approximate_current_flow_betweenness_centrality.html?highlight=betweeness)

Ces deux approches sont assez distinctes et les informations apportées par une méthode ne semblent pas directement influée sur l'autre. Nous tâcherons donc d'appliquer chacune de ces analyses avant de réfléchir aux éléments principaux qui les relient ou les séparent.

**Bases de données utilisées :**
- *dump* du Wikipédia français filtré selon JeuxDeMots
- *dump* du graphe JeuxDeMots [[3]](http://www.jeuxdemots.org/jdm-accueil.php)

**Objectifs :**
- Apprendre à nettoyer un gros jeu de données pour en extraire un réseau sémantique pertinent
- Implémenter 2 approches NLP pour en faire une analyse topologique (et si possible sémantique)
- Mettre en perspective les deux approches pour y trouver des ponts et frontières
- Explorer la perspective d'utiliser ces outils dans la recherche de modèles de la mémoire sémantique en psychologie cognitive

**À faire :** 

* [FAIT] Wikipédia gros fichiers XML (~ 20Go de texte), impossible de les charger en mémoire, nécessite un *stream-oriented parser*
* [FAIT] Utiliser les regex pour extraire du texte des pages les liens internes vers d'autres pages wiki (graphe orienté des titres de page et couples titre/texte)
* [FAIT] Stocker le tout dans une base de donnée

* [FAIT] JeuxDeMots gros fichier TXT (~ 13Go de texte), lecture ligne par ligne et extraction des lignes utiles dans des fichiers CSV.
* [FAIT] Transformer les CSV en un nouveau fichier .db

* Vectorielle : Écrire un programme (s'inspirer du jupyter-notebook du stage sur les réponses en ligne au Grand Débat)
* Vectorielle : Comment faire le plongement topologique de JeuxDeMots ?

* Graphe : Mieux maîtriser la théorie des graphes et son implémentation en Python (plus court chemin, mesures de centralités, ...)
* Graphe : *Betweenness Centrality* : comment choisir un bon *sampling* ? Voir [[4]](https://www.cc.gatech.edu/fac/Milena.Mihail/WAW07.pdf) [[5]](https://i11www.iti.kit.edu/extra/publications/bf-cmbcf-05.pdf)

* Que nous permettent de dire sémantiquement les deux approches sur les réseaux sémantiques analysés ?
* Quels résultats croisés permettent d'obtenir plus d'informations ?
* Quelles applications possibles pour les sciences cognitives ? i.e : Modèles connexionnistes de la mémoire sémantique cf [[6]](https://www.researchgate.net/publication/200045115_A_Spreading_Activation_Theory_of_Semantic_Processing)


