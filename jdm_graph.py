# Charger le graphe JeuxDeMots
import sqlite3
import networkx as nx
import numpy as np
import heapq
data = "data/jdm/base_jeuxdemots.db"
G = nx.DiGraph()
db = sqlite3.connect(data)
ite = map(lambda e: (e[0],{"valeur":e[1], "type":e[2], "poids":e[3]}), db.execute("SELECT * FROM jm_noeud"))
G.add_nodes_from(ite)
ite = map(lambda e: (e[1],e[2],{"rid":e[0], "type":e[3], "poids":e[4]}), db.execute("SELECT * FROM jm_relation"))
G.add_edges_from(ite)

# Obtenir les noeuds avec la plus forte BC parmi la plus grosse CFC
Gc = G.subgraph(max(nx.strongly_connected_components(G),key=len))
len(Gc)
# BCk = nx.betweenness_centrality(Gc,k) k = 2000 <=> 12h30, relation linéaire
BC2000 = np.load('BC2000.npy',allow_pickle='TRUE').item()
maxN = [G.nodes[i] for i in heapq.nlargest(N,BC2000,key=(lambda k:BC2000[k]))]

max30 = [{'valeur': 'médecine', 'type': 1, 'poids': 6488},
 {'valeur': 'lieu', 'type': 1, 'poids': 1456},
 {'valeur': 'géographie', 'type': 1, 'poids': 1624},
 {'valeur': 'zoologie', 'type': 1, 'poids': 1542},
 {'valeur': 'anatomie', 'type': 1, 'poids': 2730},
 {'valeur': 'droit', 'type': 1, 'poids': 1430},
 {'valeur': 'musique', 'type': 1, 'poids': 9278},
 {'valeur': 'biologie', 'type': 1, 'poids': 1810},
 {'valeur': 'chimie', 'type': 1, 'poids': 2550},
 {'valeur': 'marine', 'type': 1, 'poids': 1228},
 {'valeur': 'botanique', 'type': 1, 'poids': 1714},
 {'valeur': 'histoire', 'type': 1, 'poids': 4034},
 {'valeur': 'personne', 'type': 1, 'poids': 9836},
 {'valeur': 'cuisine', 'type': 1, 'poids': 10668},
 {'valeur': 'cinéma', 'type': 1, 'poids': 6678},
 {'valeur': 'corps', 'type': 1, 'poids': 4632},
 {'valeur': 'informatique', 'type': 1, 'poids': 2890},
 {'valeur': 'religion', 'type': 1, 'poids': 4195},
 {'valeur': 'linguistique', 'type': 1, 'poids': 2200},
 {'valeur': 'physique', 'type': 1, 'poids': 2270},
 {'valeur': 'France', 'type': 1, 'poids': 2588},
 {'valeur': 'être vivant', 'type': 1, 'poids': 1844},
 {'valeur': 'oeuvre', 'type': 1, 'poids': 1102},
 {'valeur': 'tête', 'type': 1, 'poids': 5464},
 {'valeur': 'Zoologie', 'type': 1, 'poids': 208},
 {'valeur': 'politique', 'type': 1, 'poids': 4146},
 {'valeur': 'individu', 'type': 1, 'poids': 5792},
 {'valeur': 'être humain', 'type': 1, 'poids': 738},
 {'valeur': 'militaire', 'type': 1, 'poids': 2160},
 {'valeur': 'personne>24956', 'type': 1, 'poids': 586}]

#Distribution des taille de CFC
d = list(map(len, nx.strongly_connected_components(G))) # len(d) = 127848
d10 = [7, 7, 7, 7, 7, 8, 8, 8, 9, 308891]

#Noeuds aux plus grand degré
deg = [G.nodes[i[0]] for i in sorted(G.degree, key=lambda x:x[1], reverse=True)]
deg30 = [{'valeur': 'être vivant>123933', 'type': 1, 'poids': 248},
 {'valeur': 'être>68439', 'type': 1, 'poids': 28},
 {'valeur': 'organisme vivant', 'type': 1, 'poids': 116},
 {'valeur': 'être vivant', 'type': 1, 'poids': 1844},
 {'valeur': 'organisme>71624', 'type': 1, 'poids': 66},
 {'valeur': 'personne', 'type': 1, 'poids': 9836},
 {'valeur': 'personne>24956', 'type': 1, 'poids': 586},
 {'valeur': 'entité vivante', 'type': 1, 'poids': 64},
 {'valeur': 'être humain>74860', 'type': 1, 'poids': 102},
 {'valeur': 'être humain', 'type': 1, 'poids': 738},
 {'valeur': 'zoologie', 'type': 1, 'poids': 1542},
 {'valeur': 'individu', 'type': 1, 'poids': 5792},
 {'valeur': 'médecine', 'type': 1, 'poids': 6488},
 {'valeur': 'individu>24956', 'type': 1, 'poids': 62},
 {'valeur': 'Zoologie', 'type': 1, 'poids': 208},
 {'valeur': 'homme', 'type': 1, 'poids': 28264},
 {'valeur': 'être', 'type': 1, 'poids': 808},
 {'valeur': 'ADN', 'type': 1, 'poids': 664},
 {'valeur': 'adn', 'type': 1, 'poids': 158},
 {'valeur': 'acide désoxyribonucléique', 'type': 1, 'poids': 136},
 {'valeur': 'organisme', 'type': 1, 'poids': 244},
 {'valeur': 'humain', 'type': 1, 'poids': 2274},
 {'valeur': 'être>146885', 'type': 1, 'poids': 50},
 {'valeur': 'eucaryote', 'type': 1, 'poids': 188},
 {'valeur': 'oeil', 'type': 1, 'poids': 2984},
 {'valeur': 'entité biologique', 'type': 1, 'poids': 50},
 {'valeur': 'tête', 'type': 1, 'poids': 5464},
 {'valeur': 'règne animal', 'type': 1, 'poids': 74},
 {'valeur': 'oeil>44335', 'type': 1, 'poids': 50},
 {'valeur': 'oeil>330323', 'type': 1, 'poids': 108}]

degIn = [G.nodes[i[0]] for i in sorted(G.in_degree, key=lambda x:x[1], reverse=True)]
degIn30 = [{'valeur': 'être vivant>123933', 'type': 1, 'poids': 248},
 {'valeur': 'personne>24956', 'type': 1, 'poids': 586},
 {'valeur': 'personne', 'type': 1, 'poids': 9836},
 {'valeur': 'être humain>74860', 'type': 1, 'poids': 102},
 {'valeur': 'être humain', 'type': 1, 'poids': 738},
 {'valeur': 'être>68439', 'type': 1, 'poids': 28},
 {'valeur': 'organisme vivant', 'type': 1, 'poids': 116},
 {'valeur': 'organisme>71624', 'type': 1, 'poids': 66},
 {'valeur': 'entité vivante', 'type': 1, 'poids': 64},
 {'valeur': 'individu', 'type': 1, 'poids': 5792},
 {'valeur': 'zoologie', 'type': 1, 'poids': 1542},
 {'valeur': 'être vivant', 'type': 1, 'poids': 1844},
 {'valeur': 'médecine', 'type': 1, 'poids': 6488},
 {'valeur': 'homme', 'type': 1, 'poids': 28264},
 {'valeur': 'individu>24956', 'type': 1, 'poids': 62},
 {'valeur': 'humain', 'type': 1, 'poids': 2274},
 {'valeur': 'tête', 'type': 1, 'poids': 5464},
 {'valeur': 'eucaryote', 'type': 1, 'poids': 188},
 {'valeur': 'entité biologique', 'type': 1, 'poids': 50},
 {'valeur': 'ADN', 'type': 1, 'poids': 664},
 {'valeur': 'corps', 'type': 1, 'poids': 4632},
 {'valeur': 'yeux', 'type': 1, 'poids': 4020},
 {'valeur': 'adn', 'type': 1, 'poids': 158},
 {'valeur': 'oeil>330323', 'type': 1, 'poids': 108},
 {'valeur': 'faune', 'type': 1, 'poids': 384},
 {'valeur': 'symétrie bilatérale', 'type': 1, 'poids': 6},
 {'valeur': 'acide désoxyribonucléique', 'type': 1, 'poids': 136},
 {'valeur': 'règne animal', 'type': 1, 'poids': 74},
 {'valeur': 'oeil', 'type': 1, 'poids': 2984},
 {'valeur': 'squelette>112609', 'type': 1, 'poids': 102}]

degOut = [G.nodes[i[0]] for i in sorted(G.out_degree, key=lambda x:x[1], reverse=True)]
degOut30 = [{'valeur': 'être vivant', 'type': 1, 'poids': 1844},
 {'valeur': 'être vivant>123933', 'type': 1, 'poids': 248},
 {'valeur': 'organisme vivant', 'type': 1, 'poids': 116},
 {'valeur': 'être>68439', 'type': 1, 'poids': 28},
 {'valeur': 'être', 'type': 1, 'poids': 808},
 {'valeur': 'être>146885', 'type': 1, 'poids': 50},
 {'valeur': 'organisme>71624', 'type': 1, 'poids': 66},
 {'valeur': 'entité vivante', 'type': 1, 'poids': 64},
 {'valeur': 'organisme', 'type': 1, 'poids': 244},
 {'valeur': 'zoologie', 'type': 1, 'poids': 1542},
 {'valeur': 'personne', 'type': 1, 'poids': 9836},
 {'valeur': 'personne>24956', 'type': 1, 'poids': 586},
 {'valeur': 'individu', 'type': 1, 'poids': 5792},
 {'valeur': 'être humain', 'type': 1, 'poids': 738},
 {'valeur': 'être humain>74860', 'type': 1, 'poids': 102},
 {'valeur': 'Zoologie', 'type': 1, 'poids': 208},
 {'valeur': 'individu>24956', 'type': 1, 'poids': 62},
 {'valeur': 'médecine', 'type': 1, 'poids': 6488},
 {'valeur': 'homme>24956', 'type': 1, 'poids': 160},
 {'valeur': 'sexe', 'type': 1, 'poids': 5120},
 {'valeur': 'acide désoxyribonucléique', 'type': 1, 'poids': 136},
 {'valeur': 'squelette', 'type': 1, 'poids': 1074},
 {'valeur': 'ADN', 'type': 1, 'poids': 664},
 {'valeur': 'adn', 'type': 1, 'poids': 158},
 {'valeur': 'oeil', 'type': 1, 'poids': 2984},
 {'valeur': 'eucaryote', 'type': 1, 'poids': 188},
 {'valeur': 'oeil>44335', 'type': 1, 'poids': 50},
 {'valeur': 'entité physique', 'type': 1, 'poids': 50},
 {'valeur': 'entité biologique', 'type': 1, 'poids': 50},
 {'valeur': 'règne animal', 'type': 1, 'poids': 74}]

# k = 10 : 4min30
# k = 5 : 2min
# k = 20 : 8min
# k = 40 : 16m30
# k = 320 : 2h
# k = 2000 : 12h30
