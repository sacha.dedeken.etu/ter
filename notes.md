IMéRA - "Réseaux sémantiques : construction, exploration et utilisations" (1ère partie) 12/06/17
Lien : [https://www.youtube.com/watch?v=NpiCtZmagKo](https://www.youtube.com/watch?v=NpiCtZmagKo)

Idées principales :

intervenant 1 :
- Les réseaux sémantiques séduisent car on retrouve des réseaux et de la sémantique partout et que ça peut intéresser énormément de domaines.
- Historique des réseaux sémantiques : Aristote, Arbre de Porphyre, 
* Collins & Quillian : réseau hiérarchique héritable ("Canaries can sing")
* Critique de Rosch : Que contiennent les noeuds ? -> Théorie du Prototype
* John Sowa : Concept de "conceptual graph", organisation du matériel côté machine plutôt que représentation cognitive
* Collins & Loftus : côté humain, fort aspect associationniste, les réseaux peuvent être typés, orientés et avoir un certain poids
* Modèle de Levelt : reconnaissance d'un mot par phonème->lemme->concept
* Edinburgh Associative Thesaurus : réseau associatif d'environ 8000 mots (reliés par leur capacité à apparaître en association libre)

intervenant 2 :
- Miller et al. 1990 : WordNet, réseau mot vers sens via des liens étiquetés (synonymes, hyperonymes, etc) codé à la main !
- FrameNet : cherche à déterminer le cadre sémantique (cuire : cuisinier, aliment, contenant, chaleur, ...)
- Les verbes français de Dubois & Dubois-Charlier : thésaurus de classes syntaxico-sémantiques
- Créer un réseau est un travail de moine nécessitant des choix et étant fait sur une langue qui évolue

intervenant 3 : 
- JeuxDeMots : Analyse sémantique de texte -> Algorithme de propagation sur un réseau lexico-sémantique
[- Études des GWAPs (Games With A Purpose) : 
- Amazon Mechanical Turk : crowdsourcing, production participative en micro-travail de tâches effectuées par des humains
-> Problèmes légaux, éthiques et qualitatifs
- Foldit : GWAPs en biologie et pharmacologie problème NP-complet
- Eterna : repliement de gènes
- Phylo : Phylogénétique
- Nightjar : Prédation
- Malaria Spot : GWAPs en médecine (étiquetage de leucocytes)
- Artigo : GWAPs en Art (recherche d'oeuvres par mots-clés)]
- GWAPs en NLP : 
* Wordrobe, construction d'un corpus annoté
* Zombilinguo, étiquetage de verbes
* JeuxDeMots : jeu compétitif de connaissances lexicales alimentant le réseau en regardant l'intersection des réponses données
* Emot (charge émotionnelle), I Like It (aimez-vous X ?)

How to Extract and Analyze Data from Wikipedia
Lien : [https://www.mixnode.com/tutorials/how-to-extract-and-analyze-data-from-wikipedia](https://www.mixnode.com/tutorials/how-to-extract-and-analyze-data-from-wikipedia)